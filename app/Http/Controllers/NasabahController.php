<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class NasabahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tb_nasabah')->get();

        return view('modules.nasabah.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.nasabah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nasabah_id' => 'required',
            'nasabah_nama' => 'required',
            'nasabah_alamat' => 'required',
            'nasabah_gudang' => 'required',
            'nasabah_telp' => 'required',
            'nasabah_pwd' => 'required'
        ]);

        $nasabah = DB::table('tb_nasabah')->insert([
            'nasabah_id' => $request->nasabah_id,
            'nasabah_nama' => $request->nasabah_nama,
            'nasabah_alamat' => $request->nasabah_alamat,
            'nasabah_gudang' => $request->nasabah_gudang,
            'nasabah_telp' => $request->nasabah_telp,
            'nasabah_pwd' => $request->nasabah_pwd
        ]);

        return redirect('/nasabah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('tb_nasabah')->where('nasabah_id', $id)->first();

        return view('modules.nasabah.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('tb_nasabah')->where('nasabah_id', $id)->first();

        return view('modules.nasabah.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nasabah_nama' => 'required',
            'nasabah_alamat' => 'required',
            'nasabah_gudang' => 'required',
            'nasabah_telp' => 'required',
            'nasabah_pwd' => 'required'
        ]);

        $nasabah = DB::table('tb_nasabah')->where('nasabah_id', $id)->update([
            'nasabah_nama' => $request->nasabah_nama,
            'nasabah_alamat' => $request->nasabah_alamat,
            'nasabah_gudang' => $request->nasabah_gudang,
            'nasabah_telp' => $request->nasabah_telp,
            'nasabah_pwd' => $request->nasabah_pwd
        ]);

        return redirect('/nasabah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = DB::table('tb_nasabah')->where('nasabah_id', $id)->delete();

        return redirect('/nasabah');
    }
}
