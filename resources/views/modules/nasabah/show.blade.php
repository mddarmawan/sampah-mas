@extends('layouts.app')

@section('content')
<!-- Main Wrapper -->
<div id="wrapper">

<div class="small-header">
    <div class="hpanel">
        <div class="panel-body">
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    {{-- <li><a href="index-2.html">Dashboard</a></li> --}}
                    <li>
                        <span>Nasabah</span>
                    </li>
                    <li class="active">
                        <span>Data Nasabah </span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                Data Nasabah
            </h2>
            <small>Data-data para nasabah.</small>
        </div>
    </div>
</div>

<div class="content">
	<div class="row">
	    <div class="col-lg-12">
	        <div class="hpanel">
	            <div class="panel-body">
	            	<a href="/nasabah/{{ $data->nasabah_id }}/edit"><span class="btn btn-primary btn-xs">Edit</span></a>
					<form action="/nasabah/{{ $data->nasabah_id }}" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="delete">
						<input onclick="return confirm('Anda yakin ingin menghapus?');" type="submit" class="btn btn-danger btn-xs" value="Hapus"></input>
					</form>

	                <h3>{{ $data->nasabah_nama }}</h3>
	                <p>{{ $data->nasabah_alamat }}.</p>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection