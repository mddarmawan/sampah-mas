@extends('layouts.app')

@section('content')
<!-- Main Wrapper -->
<div id="wrapper">

<div class="small-header">
    <div class="hpanel">
        <div class="panel-body">
            <div id="hbreadcrumb" class="pull-right">
                <ol class="hbreadcrumb breadcrumb">
                    {{-- <li><a href="index-2.html">Dashboard</a></li> --}}
                    <li>
                        <span>Nasabah</span>
                    </li>
                    <li class="active">
                        <span>Data Nasabah </span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                Data Nasabah
            </h2>
            <small>Data-data para nasabah.</small>
        </div>
    </div>
</div>

<div class="content">
	<div class="row">
	    <div class="col-lg-12">
	        <div class="hpanel">
	            <div class="panel-body">
	            <a href="/nasabah/create" class="btn btn-success btn-sm">Tambah Nasabah</a>
	            <div class="hr-line-dashed"></div>

	            @foreach ($data as $nasabah)
	                <h3>{{ $nasabah->nasabah_nama }} <a href="/nasabah/{{ $nasabah->nasabah_id }}"><span class="btn btn-primary btn-xs">Detail</span></a></h3>
	                <p>{{ $nasabah->nasabah_alamat }}.</p>
	            @endforeach
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection