@extends('layouts.app')

@section('content')
<!-- Main Wrapper -->
<div id="wrapper">

<div class="small-header">
	<div class="hpanel">
		<div class="panel-body">
			<div id="hbreadcrumb" class="pull-right">
				<ol class="hbreadcrumb breadcrumb">
					{{-- <li><a href="index-2.html">Dashboard</a></li> --}}
					<li>
						<span>Nasabah</span>
					</li>
					<li class="active">
						<span>Data Nasabah </span>
					</li>
				</ol>
			</div>
			<h2 class="font-light m-b-xs">
				Data Nasabah
			</h2>
			<small>Data-data para nasabah.</small>
		</div>
	</div>
</div>

<div class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="hpanel">
				<div class="panel-body">
					<h3>Tambah Data Nasabah</h3>
					<form action="/nasabah" method="post" class="form-horizontal">
						{{ csrf_field() }}
						
						<div class="hr-line-dashed"></div>
						<div class="form-group"><label class="col-sm-2 control-label">ID Nasabah</label>
							<div class="col-sm-10"><input name="nasabah_id" type="text" class="form-control"></div>
						</div>

						<div class="hr-line-dashed"></div>
						<div class="form-group"><label class="col-sm-2 control-label">Nama Nasabah</label>
							<div class="col-sm-10"><input name="nasabah_nama" type="text" class="form-control"></div>
						</div>
						
						<div class="hr-line-dashed"></div>
						<div class="form-group"><label class="col-sm-2 control-label">Alamat Nasabah</label>
							<div class="col-sm-10"><input name="nasabah_alamat" type="text" class="form-control"></div>
						</div>
						
						<div class="hr-line-dashed"></div>
						<div class="form-group"><label class="col-sm-2 control-label">Gudang Nasabah</label>
							<div class="col-sm-10"><input name="nasabah_gudang" type="number" class="form-control"></div>
						</div>
						
						<div class="hr-line-dashed"></div>
						<div class="form-group"><label class="col-sm-2 control-label">Telp. Nasabah</label>
							<div class="col-sm-10"><input name="nasabah_telp" type="text" class="form-control"></div>
						</div>
						
						<div class="hr-line-dashed"></div>
						<div class="form-group"><label class="col-sm-2 control-label">Password Nasabah</label>
							<div class="col-sm-10"><input name="nasabah_pwd" type="password" class="form-control"></div>
						</div>

						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button name="submit" class="btn btn-primary" type="submit">Simpan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection