<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Page title -->
<title>Sampah Mas</title>

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<!--<link rel="shortcut icon" type="image/ico" href="{{ asset('assets') }}/favicon.ico" />-->

<!-- Vendor styles -->
<link rel="stylesheet" href="{{ asset('assets') }}/vendor/fontawesome/css/font-awesome.css" />
<link rel="stylesheet" href="{{ asset('assets') }}/vendor/metisMenu/dist/metisMenu.css" />
<link rel="stylesheet" href="{{ asset('assets') }}/vendor/animate.css/animate.css" />
<link rel="stylesheet" href="{{ asset('assets') }}/vendor/bootstrap/dist/css/bootstrap.css" />

<!-- App styles -->
<link rel="stylesheet" href="{{ asset('assets') }}/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
<link rel="stylesheet" href="{{ asset('assets') }}/fonts/pe-icon-7-stroke/css/helper.css" />
<link rel="stylesheet" href="{{ asset('assets') }}/styles/style.css">