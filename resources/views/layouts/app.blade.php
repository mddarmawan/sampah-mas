<!DOCTYPE html>
<html lang="en">
	<head>
		@include('layouts.head')
	</head>
	<body class="light-skin fixed-navbar sidebar-scroll">
		@include('layouts.header')
		@include('layouts.nav')
		
		@yield('content')

		{{-- @include('layouts.sidebar') --}}
		@include('layouts.footer')
	</body>
</html>