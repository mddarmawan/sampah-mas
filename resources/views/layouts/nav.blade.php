<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
            <a href="{{ asset('assets') }}/index-2.html">
                <img src="{{ asset('assets') }}/images/profile.jpg" class="img-circle m-b" alt="logo">
            </a>

            <div class="stats-label text-color">
                <span class="font-extra-bold font-uppercase">Robert Razer</span>

                <div class="dropdown">
                    <a class="dropdown-toggle" href="{{ asset('assets') }}/#" data-toggle="dropdown">
                        <small class="text-muted">Founder of App <b class="caret"></b></small>
                    </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li><a href="{{ asset('assets') }}/contacts.html">Contacts</a></li>
                        <li><a href="{{ asset('assets') }}/profile.html">Profile</a></li>
                        <li><a href="{{ asset('assets') }}/analytics.html">Analytics</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ asset('assets') }}/login.html">Logout</a></li>
                    </ul>
                </div>


                <div id="sparkline1" class="small-chart m-t-sm"></div>
                <div>
                    <h4 class="font-extra-bold m-b-xs">
                        $260 104,200
                    </h4>
                    <small class="text-muted">Your income from the last year in sales product X.</small>
                </div>
            </div>
        </div>

        <ul class="nav" id="side-menu">
            <li class="active">
                <a href="/"> <span class="nav-label">Dashboard</span> <span class="label label-success pull-right">v.1</span> </a>
            </li>
            <li>
                <a href="/nasabah"><span class="nav-label">Nasabah</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li><a href="/nasabah">Data</a></li>
                    <li><a href="/nasabah/create">Tambah</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>